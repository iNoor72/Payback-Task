//
//  NavigationRoute.swift
//  Payback-Task
//
//  Created by Noor Walid on 10/08/2022.
//

import UIKit

protocol NavigationRoute {
    func navigate(to route: Route)
}

//Applied and can be used from any ViewController
extension UIViewController: NavigationRoute {
    func navigate(to route: Route) {
        switch route.style {
        case .modal:
            self.present(route.destination, animated: true, completion: nil)
        case .push:
            self.navigationController?.pushViewController(route.destination, animated: true)
        }
    }
}
