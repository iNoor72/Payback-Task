//
//  Route.swift
//  Payback-Task
//
//  Created by Noor Walid on 10/08/2022.
//

import UIKit

protocol Route {
    var destination: UIViewController { get }
    var style: NavigationStyle { get }
}

enum NavigationStyle {
    case modal
    case push
}
