//
//  DetailsViewController.swift
//  Payback-Task
//
//  Created by Noor Walid on 10/08/2022.
//

import UIKit
import Kingfisher
import AVFoundation

protocol DetailsViewProtocol: AnyObject {
    func setupViewWithImage()
    func setupViewWithVideo()
    func setupViewWithWebsite()
    func setupViewWithShoppingList()
    func setupLabels()
}

class DetailsViewController: UIViewController, DetailsViewProtocol {
    @IBOutlet private weak var tileContentView: UIView!
    @IBOutlet private weak var tileHeadlineLabel: UILabel!
    @IBOutlet private weak var contentViewHeightConstraint: NSLayoutConstraint!
    
    var detailsPresenter: DetailsPresenterProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        detailsPresenter?.showContent()
    }
    
    func setupLabels() {
        self.tileHeadlineLabel.text = detailsPresenter?.tile?.headline ?? ""
    }
    
    func setupViewWithImage() {
        guard let tileImageURL = detailsPresenter?.tile?.data else { return }
        guard let url = URL(string: tileImageURL) else { return }
        
        let imageView = UIImageView()
        imageView.kf.setImage(with: url)
        imageView.contentMode = .scaleAspectFit
        imageView.frame = self.tileContentView.bounds
        self.tileContentView.addSubview(imageView)
    }
    
    func setupViewWithVideo() {
        guard let tileVideoURL = detailsPresenter?.tile?.data else { return }
        guard let url = URL(string: tileVideoURL) else { return }
        
        let player = AVPlayer(url: url)
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = (self.tileContentView.bounds)
        playerLayer.videoGravity = .resizeAspect
        self.tileContentView.layer.addSublayer(playerLayer)
        player.play()
    }
    
    func setupViewWithWebsite() {
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(openWebsite(_:)))
        let imageView = UIImageView()
        imageView.image = UIImage(named: "website-icon")
        imageView.contentMode = .scaleAspectFit
        imageView.frame = self.tileContentView.bounds
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(singleTap)
        self.tileContentView.addSubview(imageView)
    }
    
    @objc private func openWebsite(_ sender: UIButton) {
        guard let tileWebsiteURL = detailsPresenter?.tile?.data else { return }
        guard let url = URL(string: tileWebsiteURL) else { return }
        
        UIApplication.shared.open(url)
    }
    
    func setupViewWithShoppingList() {
        let textEditor = UITextField()
        textEditor.frame = (self.tileContentView.bounds)
        textEditor.placeholder = "Add item"
        textEditor.isUserInteractionEnabled = true
        self.tileContentView.addSubview(textEditor)
    }

}
