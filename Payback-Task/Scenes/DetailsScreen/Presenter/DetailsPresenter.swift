//
//  DetailsPresenter.swift
//  Payback-Task
//
//  Created by Noor Walid on 10/08/2022.
//

import Foundation

protocol DetailsPresenterProtocol {
    var tile: Tile? { get }
    func showContent()
}

class DetailsPresenter: DetailsPresenterProtocol {
    weak var view: DetailsViewProtocol?
    var tile: Tile?
    
    init(view: DetailsViewProtocol, tile: Tile) {
        self.view = view
        self.tile = tile
    }
    
    func showContent() {
        guard let name = self.tile?.name else { return }
        self.view?.setupLabels()
        
        switch name {
        case "image":
            self.view?.setupViewWithImage()
        case "video":
            self.view?.setupViewWithVideo()
        case "website":
            self.view?.setupViewWithWebsite()
        case "shopping_list":
            self.view?.setupViewWithShoppingList()
        default:
            print("None")
        }
    }
    
    
}
