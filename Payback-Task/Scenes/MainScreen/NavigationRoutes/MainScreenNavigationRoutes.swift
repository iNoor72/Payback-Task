//
//  MainScreenNavigationRoutes.swift
//  Payback-Task
//
//  Created by Noor Walid on 10/08/2022.
//

import UIKit

enum MainScreenNavigationRoutes: Route {
    case DetailsScreen(Tile)
    
    var destination: UIViewController {
        switch self {
        case .DetailsScreen(let tile):
            guard let detailsViewController = UIStoryboard(name: Constants.Storyboards.DetailsStoryboardID, bundle: nil).instantiateViewController(withIdentifier: Constants.ViewControllers.DetailsViewControllerID) as? DetailsViewController else { return UIViewController() }
            detailsViewController.detailsPresenter = DetailsPresenter(view: detailsViewController, tile: tile)
            
            return detailsViewController
        }
    }
    
    var style: NavigationStyle {
        switch self {
        case .DetailsScreen(_):
            return .push
        }
    }
    
}
