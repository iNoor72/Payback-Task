//
//  TileTableViewCell.swift
//  Payback-Task
//
//  Created by Noor Walid on 09/08/2022.
//

import UIKit
import Kingfisher
import AVFoundation

class TileTableViewCell: UITableViewCell {
    @IBOutlet private weak var headlineLabel: UILabel!
    @IBOutlet private weak var sublineLabel: UILabel!
    @IBOutlet private weak var feedView: UIView!
    
    private var tile: Tile?
    
    override func prepareForReuse() {
        guard let name = self.tile?.name else { return }
        for view in feedView.subviews {
            view.removeFromSuperview()
        }
    }

    private func setupTileFeed(name: String) {
        switch name {
        case "image":
            self.setupViewWithImage()
        case "video":
            self.setupViewWithVideo()
            print(name)
        case "website":
            self.setupViewWithWebsite()
        case "shopping_list":
            self.setupViewWithShoppingList()
        default:
            print("None")
        }
    }
    
    private func setupViewWithImage() {
        var imageURL: URL
        if self.tile?.data != nil {
            guard let url = self.tile?.data else { return }
            imageURL = URL(string: url) ?? Constants.defaultImage
        } else {
            imageURL = Constants.defaultImage
        }
        
        
        let imageView = UIImageView()
        imageView.kf.setImage(with: imageURL)
        imageView.contentMode = .scaleAspectFit
        imageView.frame = self.feedView.bounds
        self.feedView.addSubview(imageView)
    }
    
    private func setupViewWithVideo() {
        guard let tileVideoURL = self.tile?.data else { return }
        guard let url = URL(string: tileVideoURL) else { return }
        
        let player = AVPlayer(url: url)
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = (self.feedView.bounds)
        playerLayer.videoGravity = .resizeAspect
        self.feedView.layer.addSublayer(playerLayer)
    }
    
    private func setupViewWithWebsite() {
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(openWebsite(_:)))
        let imageView = UIImageView()
        imageView.image = UIImage(named: "website-icon")
        imageView.contentMode = .scaleAspectFit
        imageView.frame = self.feedView.bounds
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(singleTap)
        self.feedView.addSubview(imageView)
    }
    
    @objc private func openWebsite(_ sender: UIButton) {
        guard let tileWebsiteURL = self.tile?.data else { return }
        guard let url = URL(string: tileWebsiteURL) else { return }
        
        UIApplication.shared.open(url)
    }
    
    private func setupViewWithShoppingList() {
        let textEditor = UITextField()
        textEditor.delegate = self
        textEditor.frame = (self.feedView.bounds)
        textEditor.placeholder = "Add item"
        textEditor.isUserInteractionEnabled = true
        self.feedView.addSubview(textEditor)
        
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        let _ = tableView.topAnchor.constraint(equalTo: textEditor.bottomAnchor)
        self.feedView.addSubview(tableView)
        
    }
    
    
    func setupCell(with tile: Tile) {
        self.tile = tile
        guard let name = self.tile?.name else { return }
        self.headlineLabel.text = self.tile?.headline ?? ""
        if self.tile?.subline != nil {
            self.sublineLabel.text = self.tile?.subline!
        } else {
            self.sublineLabel.text = ""
        }
        
        setupTileFeed(name: name)
    }
}

extension TileTableViewCell: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let itemText = textField.text else { return false }
        
        textField.resignFirstResponder()
        textField.text = ""
        return true
    }
}

extension TileTableViewCell: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        cell.textLabel?.text = "Noor"
        
        return cell
    }
}
