//
//  ViewController.swift
//  Payback-Task
//
//  Created by Noor Walid on 08/08/2022.
//

import UIKit
import Kingfisher

protocol MainViewProtocol: AnyObject, NavigationRoute {
    func reloadTableView()
}

class MainViewController: UIViewController, MainViewProtocol {
    @IBOutlet weak var tilesTableView: UITableView!
    
    private var mainPresenter: MainViewPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        mainPresenter = MainViewPresenter(view: self)

        NotificationCenter.default.addObserver(forName: .NSCalendarDayChanged, object: nil, queue: .main) {[weak self] _ in
            guard let self = self else { return }

            self.mainPresenter.fetchTilesFromNetwork()
        }
        
        mainPresenter.fetchTilesFromDatabase()
    }
    
    private func setupTableView() {
        [tilesTableView].forEach {
            $0?.delegate = self
            $0?.dataSource = self
        }
        
        tilesTableView.register(UINib(nibName: Constants.XIBs.TileTableViewCellXIB, bundle: nil), forCellReuseIdentifier: Constants.TableViewCells.TileTableViewCellID)
    }
    
    func reloadTableView() {
        DispatchQueue.main.async {[weak self] in
            self?.tilesTableView.reloadData()
        }
    }

}

//MARK: Extensions

//MARK: UITableView
extension MainViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mainPresenter.tiles?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tilesTableView.dequeueReusableCell(withIdentifier: Constants.TableViewCells.TileTableViewCellID, for: indexPath) as? TileTableViewCell else { return UITableViewCell() }
        
        cell.setupCell(with: mainPresenter.tiles?[indexPath.row] ?? Tile())
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        mainPresenter.navigateToTile(at: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(250)
    }
}

