//
//  MainScreenRepository.swift
//  Payback-Task
//
//  Created by Noor Walid on 09/08/2022.
//

import Foundation

class MainScreenRepository {
    private let cache: DatabaseProtocol
    private let network: NetworkServiceProtocol
    
    init(network: NetworkServiceProtocol, cache: DatabaseProtocol) {
        self.network = network
        self.cache = cache
    }
    
    func fetchTilesFromNetwork(onFetch: @escaping (Result<TilesResponse, Error>) -> Void) {
        network.fetchData(url: .tiles, expectedType: TilesResponse.self) { result in
            switch result {
            case .failure(let error):
                onFetch(.failure(error))
                
            case .success(let reponse):
                onFetch(.success(reponse))
            }
        }
    }
    
    func fetchTilesFromDatabase(onFetch: @escaping (Result<[Tile], Error>) -> Void) {
        cache.fetch { tiles in
            onFetch(.success(tiles))
        }
    }
    
    func saveTilesToDatabase(tile: Tile) {
        cache.save(tile: tile)
    }
    
    func isDatabaseEmpty() -> Bool {
        var result = false
        cache.fetch { tiles in
            if tiles.isEmpty {
                result = true
            } else {
                result = false
            }
        }
        
        return result
    }
}
