//
//  MainViewPresenter.swift
//  Payback-Task
//
//  Created by Noor Walid on 09/08/2022.
//

import Foundation

protocol MainViewPresenterProtocol {
    var tiles: [Tile]? { get }
    func fetchTilesFromNetwork()
    func fetchTilesFromDatabase()
    func navigateToTile(at index: Int)
}

class MainViewPresenter: MainViewPresenterProtocol {
    private weak var view: MainViewProtocol?
    private let repository: MainScreenRepository
    
    var tiles: [Tile]?
    
    init(view: MainViewProtocol, repository: MainScreenRepository = MainScreenRepository(network: NetworkManager.shared, cache: RealmDatabaseManager.shared)) {
        self.view = view
        self.repository = repository
    }
    
    func fetchTilesFromNetwork() {
        repository.fetchTilesFromNetwork { [weak self] result in
            guard let self = self else { return }
            
            switch result {
            case .failure(let error):
                print("Error! \(error)")
                
            case .success(let response):
                let tilesArray = Array(response.tiles)
                self.tiles = tilesArray
                if self.repository.isDatabaseEmpty() {
                    self.saveTilesToDatabase(tiles: tilesArray)
                }
                self.view?.reloadTableView()
                print(response)
            }
        }
    }
    
    func fetchTilesFromDatabase() {
        repository.fetchTilesFromDatabase {
            [weak self] result in
            guard let self = self else { return }
            
            switch result {
            case .failure(let error):
                print("Error! \(error)")
                
            case .success(let tiles):
                if tiles.count == 0 {
                    self.fetchTilesFromNetwork()
                }
                self.tiles = tiles
                self.view?.reloadTableView()
                print(tiles)
            }
        }
    }
    
    func navigateToTile(at index: Int) {
        let route = MainScreenNavigationRoutes.DetailsScreen(self.tiles?[index] ?? Tile())
        self.view?.navigate(to: route)
    }
    
    private func saveTilesToDatabase(tiles: [Tile]) {
        for tile in tiles {
            repository.saveTilesToDatabase(tile: tile)
        }
    }
}
