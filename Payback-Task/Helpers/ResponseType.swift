//
//  ResponseType.swift
//  Payback-Task
//
//  Created by Noor Walid on 08/08/2022.
//

import Foundation

enum ResponseType: String {
    case tiles = "tiles"
}
