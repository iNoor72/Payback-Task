//
//  RealmDatabaseManager.swift
//  Payback-Task
//
//  Created by Noor Walid on 10/08/2022.
//

import Foundation
import RealmSwift

class RealmDatabaseManager: DatabaseProtocol {
    static let shared = RealmDatabaseManager()
    
    private let localRealm = try! Realm()
    
    private init() {
        print(Realm.Configuration.defaultConfiguration.fileURL!)
    }

    func save(tile: Tile) {
        try? localRealm.write {
            self.localRealm.add(tile)
        }
    }
    
    func delete(tile: Tile) {
        try? localRealm.write {
            self.localRealm.delete(tile)
        }
    }
    
    func deleteAll() {
        var tilesArray = [Tile]()
        self.fetch { tiles in
            tilesArray = tiles
        }
        
        try? localRealm.write {
            localRealm.delete(tilesArray)
        }
    }
    
    func fetch(completion: @escaping (([Tile]) -> ())) {
        let tiles = localRealm.objects(Tile.self)
        let sortedTiles = tiles.sorted(byKeyPath: "score", ascending: false)
        let tilesArray = Array(sortedTiles)
        completion(tilesArray)
    }
}
