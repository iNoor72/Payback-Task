//
//  NetworkRouter.swift
//  Payback-Task
//
//  Created by Noor Walid on 08/08/2022.
//

import Foundation
import Alamofire

enum NetworkRouter: URLRequestConvertible {
    case tiles

    var path: String {
        switch self {
        case .tiles:
            return "/feed.json"
        }
    }
    
    
    var method: HTTPMethod {
            switch self {
            case .tiles:
                return .get
            }
        }
        
    //We don't need headers but if needed, uncomment the code and write the headers
    
//    var headers: [String:String] {
//        switch self {
//        case .books(_):
//            return ["":""]
//        }
//    }

    
        var parameters: [String: Any] {
            switch self {
            case .tiles:
                return ["alt":"media", "token":"3b3606dd-1d09-4021-a013-a30e958ad930"]
            }
        }
    
    func asURLRequest() throws -> URLRequest {
        guard var safeURL = URL(string: Constants.baseURL) else { return URLRequest(url: Constants.dummyURL) }
        safeURL.appendPathComponent(path)
        var request = URLRequest(url: safeURL)
        request.method = method
        switch self {
        default:
            request = try URLEncoding.default.encode(request, with: parameters)
        }
        print(request)
        return request
    }
}
