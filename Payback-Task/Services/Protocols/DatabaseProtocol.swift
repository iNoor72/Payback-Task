//
//  DatabaseProtocol.swift
//  Payback-Task
//
//  Created by Noor Walid on 10/08/2022.
//

import Foundation

protocol DatabaseProtocol {
    func save(tile: Tile)
    func delete(tile: Tile)
    func deleteAll()
    func fetch(completion: @escaping (([Tile]) -> ()))
}
