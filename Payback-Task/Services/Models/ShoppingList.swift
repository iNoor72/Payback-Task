//
//  ShoppingList.swift
//  Payback-Task
//
//  Created by Noor Walid on 13/08/2022.
//

import Foundation
import RealmSwift

class ShoppingList: Object {
    @Persisted var items: List<Item>
}

class Item: Object {
    @Persisted var itemName: String
}
