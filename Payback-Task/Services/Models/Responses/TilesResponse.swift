//
//  TilesResponse.swift
//  Payback-Task
//
//  Created by Noor Walid on 09/08/2022.
//

import Foundation
import RealmSwift

class TilesResponse: Object, Codable {
    @Persisted var tiles: List<Tile>
}

class Tile: Object, Codable {
    @Persisted var name: String?
    @Persisted var headline: String?
    @Persisted var subline: String?
    @Persisted var data: String?
    @Persisted var score: Int?
}
