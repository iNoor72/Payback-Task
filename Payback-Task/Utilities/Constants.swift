//
//  Constants.swift
//  Payback-Task
//
//  Created by Noor Walid on 08/08/2022.
//

import Foundation

struct Constants {
    static let baseURL = "https://firebasestorage.googleapis.com/v0/b/payback-test.appspot.com/o"
    static let dummyURL = URL(string:"https://google.com")!
    static let defaultImage = URL(string: "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/1024px-No_image_available.svg.png")!
    
    struct ViewControllers {
        static let DetailsViewControllerID = "DetailsViewController"
    }
    
    struct TableViewCells {
        static let TileTableViewCellID = "TileTableViewCell"
    }
    
    struct XIBs {
        static let TileTableViewCellXIB = "TileTableViewCell"
    }
    
    struct Storyboards {
        static let MainStoryboardID = "Main"
        static let DetailsStoryboardID = "Details"
    }
}
