# Payback-Task

Payback-Task is a project build for Payback company.

## Description
This is my implementation for the task requested by Payback company. An iOS tiles app similar to Payback's tiles screen in Payback's iOS app to fetch tiles connected to Firebase API with some cool features.
It's an iOS app that shows different types of tiles to the user where the user can interact with them.

## Getting started
To run the project, clone the project and run the following commands:

```
cd "The path to the project on your machine"
pod install
```

## Features
- List the result tiles for the user in the Main screen in a descending order based on the score fetched for each tile.
- Detailed cell to show the headline and the subline (if found) of the tile in a good looking border.
- Detailed screen for each tile when the user taps on the tile.
- MVP architecture with Clean Code principles.
- Persisting the data for the user and loaded once every day from the network.
- Supporting iOS 15.5 and newer.
- Supporting all of the iPhone devices in the Portrait mode.

## Pods
- Alamofire: Used for all of the heavy work of networking in the app.
- Kingfisher: Used for fetching images for the books, totally built on Alamofire.
- RealmSwift: Used for saving and persisting the books for the user.

## Project status
The project is completed in what was mentioned in the requirements document, however, the tile cell in the Main screen is missing better looking for the video and shopping list tiles, there's some logic missing for handling the loading of data for the tile but the base is there.

If I had a bit of time I could have been able to finish it but due to the deadline being today I wasn't been able to complete it.
